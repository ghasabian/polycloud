
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class CompetingConsumerServer {
	
	public static void main(String[] args) throws IOException {

		//========DATA CONNECTION=============//
		final int port = 9090;
		System.out.println("Server waiting for connection on port "+port);
		ServerSocket ss = new ServerSocket(port);
        while (true) {
             Socket clientSocket = ss.accept();
		     System.out.println("Recieved connection from "+clientSocket.getInetAddress()+" on port "+clientSocket.getPort());
		
			RecieveFromClientThread3 recieve = new RecieveFromClientThread3(clientSocket);
			Thread thread = new Thread(recieve);
			thread.start();
			
        }
	}}

class RecieveFromClientThread3 implements Runnable
{
	static String masterurl = String.format("jdbc:mysql://%s:%d/%s", "ec2-52-201-249-75.compute-1.amazonaws.com", 3306, "sakila");																 
	static String slave1url = String.format("jdbc:mysql://%s:%d/%s", "ec2-54-165-57-113.compute-1.amazonaws.com", 3306, "sakila");
	static String slave2url = String.format("jdbc:mysql://%s:%d/%s", "ec2-54-88-34-184.compute-1.amazonaws.com", 3306, "sakila");
	static String slave3url = String.format("jdbc:mysql://%s:%d/%s", "ec2-52-201-249-75.compute-1.amazonaws.com", 3306, "sakila");
	
	//======
	
	static Connection conmaster = null;
	static Connection conSlave1 = null;
	static Connection conSlave2 = null;
	static Connection conSlave3 = null;
	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conmaster = (Connection) DriverManager.getConnection(masterurl, "root", "");
			conSlave1 = (Connection) DriverManager.getConnection(slave1url, "root", "");
			conSlave2 = (Connection) DriverManager.getConnection(slave2url, "root", "");
			conSlave3 = (Connection) DriverManager.getConnection(slave3url, "root", "");
		} catch (Exception e) {
			e.printStackTrace();
		} 	
	}
	
	Socket clientSocket=null;
	PrintWriter pwPrintWriter;
	BufferedReader brBufferedReader = null;
	
	public RecieveFromClientThread3(Socket clientSocket)
	{
		this.clientSocket = clientSocket;
	}
	
	public void run() {
		try{
         
        pwPrintWriter =new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()), true);//get outputstream
		brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
		
		String messageString;
		while((messageString = brBufferedReader.readLine())!= null){
			System.out.println("Client accepted: " + this.clientSocket.getRemoteSocketAddress().toString());
			if(messageString.equals("QUIT"))
			{
				break;
			}
			
			String query =messageString.split("ENCODE")[0];
			String encode = messageString.split("ENCODE")[1];
			try {
				System.out.println(query);
				executeMyQuery(query, encode);
				pwPrintWriter.println("result:OK");
	         }catch (Exception err) {
	            err.printStackTrace();
	            pwPrintWriter.println("result:NotOk");
	        }
		}
		
		pwPrintWriter.close();
		brBufferedReader.close();
		this.clientSocket.close();
		
	}
	catch(Exception ex){
		ex.printStackTrace();
		}
	}

	private void executeMyQuery(String query, String encode) throws SQLException {
		Statement statement = null;
	    int firstDigit = Integer.valueOf(encode.substring(encode.length() - 1,encode.length()));
	    if(firstDigit < 3){
	    	statement = conSlave1.createStatement();
	    }
	    else if(firstDigit>=3 && firstDigit <6){
	    	statement = conSlave2.createStatement();
	    }
	    else if(firstDigit>6 && firstDigit <=8){
	    	statement = conSlave3.createStatement();
	    }
	    else{
	    	statement = conmaster.createStatement();
	    }
	    statement.executeUpdate(query);
	}
}

