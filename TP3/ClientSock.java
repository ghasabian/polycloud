import java.io.*;
import java.net.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import logic.MQRestCaller;


public class ClientSock extends Thread {
	
	//public ClientSock(){}
	public static int numClients = 1000;  //Number of clients: 100, 250, 500 and 1000  
	public static void main(String[] args) {
		
		for(int a = 0; a< numClients; ++a){
			Map<Integer, ClientSock> clients = new HashMap<Integer, ClientSock>();
			ClientSock client = new ClientSock();
			clients.put(a, client);
			client.start();
			//System.out.println(client);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
	
	
  public void run(){
		try {
			Socket sock = new Socket("ec2-54-86-6-177.compute-1.amazonaws.com", 8000);  // GK Server

			InputStream inputStream = sock.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

			OutputStream outputStream = sock.getOutputStream();
			PrintWriter wr = new PrintWriter(new OutputStreamWriter(outputStream), true);
        
			int numberOfLoops = 5;
	        long start = -System.nanoTime();
	        
			synchronized(sock){
				//SYNC CLIENTS HERE 
					 for (int j=0; j<numberOfLoops;j++){ //five times read
						wr.println("select count(*) from film;");   //READ REQUEST
			
						String rdr;
						while ((rdr = br.readLine()) != null) {
							System.out.println("rdr:" + rdr);
							break;
						}
				  }//end forloop
					 
					 try {
							Thread.sleep(5000);                     //WAIT 5 SECONDS
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					 numberOfLoops=5;    //READ REQUESTR 100 TIMES
					 for (int j=0; j<numberOfLoops;j++){ //five times read
							wr.println("select count(*) from film;");   //READ REQUEST
				
							String rdr;
							while ((rdr = br.readLine()) != null) {
								System.out.println("rdr:" + rdr);
								break;
							}
					  }//end forloop
						 
						 try {
								Thread.sleep(5000);                     //WAIT 5 SECONDS
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						 
						 numberOfLoops=5;
						 for (int j=0; j<numberOfLoops;j++){ //five times write
								wr.println("insert into film (title, description, release_year, language_id)" + " "
										+ "VALUES ('sample_movie', 'This is just a test', 2016, 1);");   
								String rdr;
								while ((rdr = br.readLine()) != null) {
									System.out.println("rdr:" + rdr);
									break;
								}
						 }	 
				 
		}//END SYNC
			
		   //CALCULATE TIME 				
			start += System.nanoTime();
	        System.out.println("Time for "+numClients+" Clients CRU-D with random proxy is: "+ ((start / 1000000) / numberOfLoops) + "ms");
				 
			br.close();
			wr.close();
			sock.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}//end run
	
}

