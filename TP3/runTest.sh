

#!/bin/bash


X=1
while [ $X -gt 0 ]
do
var=$(ps -ef|grep mysql|awk '{ print $2 }'|head -n1)
if [ -n "$var" ]
then
X=0
fi
done
touch runslave.txt
cd ~/powerapi/bin
sudo ./powerapi modules procfs-cpu-simple monitor --frequency 1000 --pids $var --agg sum --file  ~/runslave.txt duration 500

cd /home/ubuntu

totalenergy=0
for line in `cat runslave.txt`
do
energy=`echo $line | sed -e 's/.*power=//g'`
totalenergy=`echo $totalenergy + $energy | bc `
done
echo 'Total energy is: ' > Result.txt
echo $totalenergy >> Result.txt
sudo rm runslave.txt
~                             
