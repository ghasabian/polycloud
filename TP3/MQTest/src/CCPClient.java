
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.UUID;

import logic.MQRestCaller;

public class CCPClient extends Thread {
		public static int numClients = 1000;    //Number of clients: 100, 250, 500 and 1000  
		public static void main(String[] args) {
			
			for(int a = 0; a<numClients; ++a){     
				CCPClient client = new CCPClient();
				client.start();
				//System.out.println(client.toString());
			}
		}
		
	  public void run(){
			try {
				Socket sock = new Socket("localhost", 9090);  //

				InputStream inputStream = sock.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

				OutputStream outputStream = sock.getOutputStream();
				PrintWriter wr = new PrintWriter(new OutputStreamWriter(outputStream), true);
	            
			    UUID uid = MQRestCaller.PlaceRequest("placeIntoMessageQueueRandomProxy");
			    int res = Integer.valueOf(MQRestCaller.GetFromMessageQueue(uid).toString());
			    
			    long r = Math.abs(uid.getMostSignificantBits()) / res;
			    String encode = "" + r;
			    
			    int numberOfLoops = 5;
		        long start = -System.nanoTime();
			    
				 for (int j=0; j<numberOfLoops;j++){ //five times write
					wr.println("insert into film (title, description, release_year, language_id)" + " "
							+ "VALUES ('sample_movie', 'This is just a test', 2016, 1);ENCODE" + encode);   //select release_year from film where film_id = 500;
					String rdr;
					while ((rdr = br.readLine()) != null) {
						System.out.println("rdr:" + rdr);
						break;
					}
			
			  }
			 //CALCULATE TIME 				
		      start += System.nanoTime();
			  System.out.println("Time for "+numClients+" Clients inerting with random proxy is: "+ ((start / 1000000) / numberOfLoops) + "ms");	
				br.close();
				wr.close();
				sock.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
}





