import java.io.*;
import java.net.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import logic.MQRestCaller;


public class ClientSock extends Thread {
	
	//public ClientSock(){}
	
	public static void main(String[] args) {
		//int a=0;
		for(int a = 0; a< 10; ++a){
			Map<Integer, ClientSock> clients = new HashMap<Integer, ClientSock>();
			ClientSock client = new ClientSock();
			clients.put(a, client);
			client.start();
			System.out.println(client);
		}
		
		
	}
	
	
  public void run(){
		try {
			Socket sock = new Socket("localhost", 8000);  //

			InputStream inputStream = sock.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

			OutputStream outputStream = sock.getOutputStream();
			PrintWriter wr = new PrintWriter(new OutputStreamWriter(outputStream), true);
            
		
			
			 for (int j=0; j<5;j++){ //five times read and write
				wr.println("select count(*) from film;");   //select release_year from film where film_id = 500;
	
				String rdr;
				while ((rdr = br.readLine()) != null) {
					System.out.println("rdr:" + rdr);
					break;
				}
		  }//end forloop
			 
			br.close();
			wr.close();
			sock.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}//end run
	
}

