
import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;
import static logic.MQRestCaller.SelectSimple;
import static logic.MQRestCaller.PlaceRequest;
import static logic.MQRestCaller.GetFromMessageQueue;

public class MQPerformanceTest {

    public MQPerformanceTest() {
    }

    @Test
    public void RandomQueryWithMessageQueueRandomProxy() {
        int numberOfLoops = 5;
        int numQueries = 1000;
        long start = -System.nanoTime();
        for (int j = 0; j < numberOfLoops; j++) {
            for (int i = 0; i < numQueries; i++) {
                UUID uid = PlaceRequest("placeIntoMessageQueueRandomProxy");
                Object res = GetFromMessageQueue(uid);

                assertNotNull(res);
            }
        }

        start += System.nanoTime();
        System.out.println("Time for " + numQueries + " simple select with random proxy: "
                + (start / 1000000) / numberOfLoops + "ms");
    }

}
