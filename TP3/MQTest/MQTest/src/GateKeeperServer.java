
import java.io.*;
import java.net.*;
//import java.lang.*;

public class GateKeeperServer {

    public static void main(String[] args) throws IOException {
        System.out.println("gatekeeper is going to start: ");
        final int port = 8000;

        int Threshold = 5;
        int counter = 0;

        System.out.println("Server waiting for connection on port " + port);
        ServerSocket ss = new ServerSocket(port);

        while (true) {
            Socket clientSocket = ss.accept();
            System.out.println("Recieved connection from " + clientSocket.getInetAddress() + " on port " + clientSocket.getPort());

            RecieveFromClientThread1 recieve = new RecieveFromClientThread1(clientSocket);
            Thread thread = new Thread(recieve);
            thread.start();
        }

    }
}

class RecieveFromClientThread1 implements Runnable {

    Socket clientSocket = null;
    PrintWriter pwPrintWriter;
    BufferedReader brBufferedReader = null;
    private Socket gateKeepertoThSocket;

    public RecieveFromClientThread1(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try {
            gateKeepertoThSocket = new Socket("localhost", 9000);
            PrintWriter gkPrintWriter = new PrintWriter(new OutputStreamWriter(gateKeepertoThSocket.getOutputStream()), true);//get outputstream
            BufferedReader gkbrBufferedReader = new BufferedReader(new InputStreamReader(gateKeepertoThSocket.getInputStream()));

            pwPrintWriter = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()), true);//get outputstream
            brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            String messageString;
            while ((messageString = brBufferedReader.readLine()) != null) {//assign message from client to messageString
                if (messageString.equals("QUIT")) {
                    break;//break to close socket if EXIT
                }
                gkPrintWriter.println(messageString);
                String gkmessageString;
                while ((gkmessageString = gkbrBufferedReader.readLine()) != null) {//assign message from client to messageString
                    System.out.println("gkmessageString received from TrustedHost is:" + gkmessageString);
                    break;
                }
                pwPrintWriter.println(gkmessageString);
            }

            pwPrintWriter.close();
            brBufferedReader.close();

            gkPrintWriter.close();
            gkbrBufferedReader.close();

            this.clientSocket.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}//end class RecieveFromClientThread
