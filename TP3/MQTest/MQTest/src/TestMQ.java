
import java.util.UUID;

import logic.MQRestCaller;

public class TestMQ {

    public static void main(String[] args) {

        int lessThan4 = 0, betwee4And7 = 0, moreThan7 = 0;

        for (int a = 0; a < 1000; ++a) {

            UUID uid = MQRestCaller.PlaceRequest("placeIntoMessageQueueRandomProxy");
            Object res = MQRestCaller.GetFromMessageQueue(uid);

            //System.out.println(uid.toString() + res);
            long r = Math.abs(uid.getMostSignificantBits()) / 2006;
            System.out.println(r);
            String str = "" + r;

            int firstDigit = Integer.valueOf(str.substring(str.length() - 1, str.length()));

            System.out.println(firstDigit);

            if (firstDigit < 3) {
                lessThan4++;
            } else if (firstDigit >= 3 && firstDigit < 6) {
                betwee4And7++;
            } else {
                moreThan7++;
            }

        }

        System.out.println(String.format("lessThan4 = %s, betwee4And7 = %s, moreThan7 = %s", lessThan4, betwee4And7, moreThan7));

    }

}
