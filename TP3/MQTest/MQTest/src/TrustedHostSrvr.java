
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TrustedHostSrvr {

    public static void main(String[] args) throws IOException {

        //========DATA CONNECTION=============//
        final int port = 9000;
        System.out.println("Server waiting for connection on port " + port);
        ServerSocket ss = new ServerSocket(port);

        while (true) {
            Socket clientSocket = ss.accept();
            System.out.println("Recieved connection from " + clientSocket.getInetAddress() + " on port " + clientSocket.getPort());
            //create two threads to send and recieve from client
            RecieveFromClientThread2 recieve = new RecieveFromClientThread2(clientSocket);
            Thread thread = new Thread(recieve);
            thread.start();

        }
    }
}

class RecieveFromClientThread2 implements Runnable {

    Socket clientSocket = null;
    PrintWriter pwPrintWriter;
    BufferedReader brBufferedReader = null;

    public RecieveFromClientThread2(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }//end constructor

    public void run() {
        try {

            pwPrintWriter = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()), true);//get outputstream
            brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            //String ip = this.clientSocket.getRemoteSocketAddress().toString();
            String messageString; //, gkmessageString;
            //while(true){
            while ((messageString = brBufferedReader.readLine()) != null) {//assign message from client to messageString
                System.out.println("Client accepted: " + this.clientSocket.getRemoteSocketAddress().toString());
                if (messageString.equals("QUIT")) {
                    break;//break to close socket if QUIT
                }
                System.out.println("From Client: " + messageString);//print the message from client

                pwPrintWriter.println("connect to db and return the result!!!");
                String url = String.format("jdbc:mysql://%s:%d/%s", "ec2-54-89-114-99.compute-1.amazonaws.com", 3306, "sakila");
                Connection conn = null;
                ResultSet rs = null;
                Statement statement = null;
                System.out.println("connect to mysql db ...");
                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    conn = (Connection) DriverManager.getConnection(url, "root", "");

                    statement = (Statement) conn.createStatement();
                    rs = statement.executeQuery(messageString);

                    while (rs.next()) {
                        // rs.first();
                        int i = 1;
                        //System.out.println(rs.getString("title" ));
                        System.out.println(rs.getString(i++));
                    }

                    //System.out.println(rs.getString("title"));
                } catch (Exception err) {
                    err.printStackTrace();
                }

            }
            pwPrintWriter.close();
            brBufferedReader.close();
            this.clientSocket.close();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}//end class RecieveFromClientThread
