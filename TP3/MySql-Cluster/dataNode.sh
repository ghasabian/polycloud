#!/bin/sh

####################################
# DATA Node
####################################

export DEBIAN_FRONTEND=noninteractive
sudo rm -rf /var/lib/mysql
sudo rm -rf /etc/mysql/

sudo apt-get -y remove mysql*
sudo apt-get -y --purge remove
sudo apt-get -y autoremove
sudo dpkg --get-selections | grep mysql
sudo aptitude -y purge $(dpkg --get-selections | grep deinstall | sed s/deinstall//)

sudo apt-get -y install libaio1 libaio-dev

sudo groupadd mysql
sudo useradd -g mysql mysql

cd /var/tmp/

sudo wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo tar -xvzf mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo cp /var/tmp/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64/bin/ndbd /usr/local/bin/ndbd

sudo cp /var/tmp/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64/bin/ndbmtd /usr/local/bin/ndbmtd


sudo mkdir -p /usr/local/mysql/data
sudo rm -f /etc/mysql/my.cnf
sudo rm -f /usr/local/mysql/my.cnf

sudo cat > /etc/my.cnf << EOF

#####################################
# Config for  SQL Node & Data Nodes #
#####################################
#
#
#  /etc/my.cnf
#
#

[mysqld]
ndbcluster                      


[mysql_cluster]
ndb-connectstring=master  

EOF

ndbd
