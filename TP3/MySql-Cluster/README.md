##############################################
#  ARMSTRONG, ALI AND CEDRIK
#
##############################################
#       A SIMPLE CLUSTER DESIGN              #
##############################################
# master = mgmnt_node                        #
# node01 = data_node			             #
# node02 = date_node                         #
# node03 = data_node 			             #
# node04 = sql_node			                 #
#                                            #
##############################################

##############################################
# Config Location for  SQL Node & Data Nodes #
##############################################
#
#     sudo vim     /etc/my.cnf
#
##############################################
#  Config File Location For Management Node  #
##############################################
#
#  
#  sudo vim /var/lib/mysql-cluster/config.ini
#
##############################################


##############################################
#  INFO on files in this Directory           #
##############################################
#        
# Run this setup script on each  data nodes
#  		DataNode.sh 
# Run this setup script on the management node                  
#  		ManagementNode.sh
# Run thus setup script on SQL nodes             
#  		SQLNode.sh                  
#
###############################################



#####################################
#####  Running & Testing  ###########
#####################################


#STARTUP ORDER
##############

#IMPORTANT! Startup order matters.
#Start nodes in the following order.


#1) Management / Master node
#2) Data nodes
#3) SQL nodes


#STARTUP COMMANDS
#################

# 1) Starting The Management Node:
# /> ndb_mgmd -f /var/lib/mysql-cluster/config.ini

Note: Configuration file should be pointed 
(using -f ) in the initial start up only.   

# 2) Starting The Data Nodes:
#    /> ndbd

# 3) Starting The SQL Nodes 
#    /> pkill -9 mysql
#    /> sudo service mysql.server start

#CHECK YOUR CLUSTER
#######################################
#root@master:~# ndb_mgm
#-- NDB Cluster -- Management Client --
#ndb_mgm> show




######################################################################### 
# SQL User setup - you might want access external from the cluster      #
#########################################################################
#
# mysql -h node001 -u root
#
# CREATE USER 'root'@'%' IDENTIFIED BY '';
# GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
#
# you can now connect to a cluster sql node from scripts outside data_node




