#!/bin/sh


#####################################
#       Management Node             #   
#####################################

export DEBIAN_FRONTEND=noninteractive
sudo rm -rf /var/lib/mysql
sudo rm -rf /etc/mysql/
sudo rm -f /etc/mysql/my.cnf

sudo apt-get -y remove mysql*
sudo apt-get -y --purge remove
sudo apt-get -y autoremove
sudo dpkg --get-selections | grep mysql
sudo aptitude -y purge $(dpkg --get-selections | grep deinstall | sed s/deinstall//)


cd /var/tmp/


wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo tar -xvzf mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo cp /var/tmp/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64/bin/ndb_mgm* /usr/local/bin/


sudo rm -rf /var/tmp/*

sudo mkdir -p /var/lib/mysql-cluster/

sudo cat > /var/lib/mysql-cluster/config.ini << EOF

#####################################
#  Config File For Management Node  #
#####################################
#
# /var/lib/mysql-cluster/config.ini
#####################################

[ndbd default]
# Options affecting ndbd processes on all data nodes:
NoOfReplicas=2    # Number of replicas
DataMemory=80M    # How much memory to allocate for data storage
IndexMemory=18M   # How much memory to allocate for index storage
# For DataMemory and IndexMemory, we have used the
# default values. Since the "world" database takes up
# only about 500KB, this should be more than enough for
# this example Cluster setup.

[tcp default]
# TCP/IP options:
portnumber=2202   # This the default; however, you can use any
# port that is free for all the hosts in the cluster
# Note: It is recommended that you do not specify the port
# number at all and simply allow the default value to be used
# instead

[ndb_mgmd]
# Management process options:
hostname=ip-172.31.25.142.ec2.internal          # Hostname or IP address of MGM node
datadir=/var/lib/mysql-cluster                  # Directory for MGM node log files
nodeid=1

[ndbd]
# Options for data node "A":
# (one [ndbd] section per data node)
hostname=ip-172-31-25-143.ec2.internal          # Hostname or IP address
datadir=/usr/local/mysql/data                   # Directory for this data node's data files
nodeid=2

[ndbd]
# Options for data node "B":
hostname=ip-172.31.25.140.ec2.internal          # Hostname or IP address
datadir=/usr/local/mysql/data                   # Directory for this data node's data files
nodeid=3

[ndbd]
# Options for data node "C":
hostname=ip-172-31-25-141.ec2.internal          # Hostname or IP address
datadir=/usr/local/mysql/data                   # Directory for this data node's data files
nodeid=4

[mysqld]
# SQL node options:
datadir=/var/lib/mysql-cluster                  # Directory for MGM node log files
hostname=ip-172-31-25-142.ec2.internal                              # Hostname or IP address = localhost
nodeid=5                                        # (additional mysqld connections can be
# specified for this node for various
# purposes such as running ndb_restore)

EOF

ndb_mgmd --configdir=/var/lib/mysql-cluster/ -f /var/lib/mysql-cluster/config.ini




