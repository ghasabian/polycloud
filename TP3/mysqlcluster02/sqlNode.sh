#!/bin/sh

####################################
#             SQL Node             #
####################################


sudo export DEBIAN_FRONTEND=noninteractive
sudo rm -rf /var/lib/mysql
sudo rm -rf /etc/mysql/
sudo apt-get -y remove mysql*
sudo apt-get -y --purge remove
sudo apt-get -y autoremove
sudo dpkg --get-selections | grep mysql
sudo aptitude -y purge $(dpkg --get-selections | grep deinstall | sed s/deinstall//)


sudo apt-get -y install libaio1 libaio-dev

sudo groupadd mysql
sudo useradd -g mysql mysql


cd /var/tmp/

sudo wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo tar -xvzf mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz

sudo cp -r /var/tmp/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64/ /usr/local/

sudo ln -s /usr/local/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64/ /usr/local/mysql-cluster


sudo cat > /etc/my.cnf << EOF

#####################################
# Config for  SQL Node & Data Nodes #
#####################################
#
#
#  /etc/my.cnf
#

[mysqld]
ndbcluster                      


[mysql_cluster]
ndb-connectstring=172.31.42.18  

[mysqld]
basedir=/usr/local/mysql-cluster/

EOF

#NOTE: About non-default installs.
#Moving database away from default location /var/lib/mysql will generate errors when
#running mysql_install_db, until you,
#update the usr.sbin.mysql file in /etc/apparmor.d/ , then run 
#/etc/init.d/apparmor restart



sudo cat > /etc/apparmor.d/usr.sbin.mysqld << EOF

/usr/local/mysql-cluster/data/ r,
/usr/local/mysql-cluster/data/** rwk,


EOF

service apparmor reload



/usr/local/mysql-cluster/scripts/mysql_install_db --user=mysql --basedir=/usr/local/mysql-cluster/ --datadir=/usr/local/mysql-cluster/data/ --defaults-file=/etc/my.cnf 


sudo cp /usr/local/mysql-cluster/bin/mysqld_safe /usr/bin/
sudo cp /usr/local/mysql-cluster/bin/mysql /usr/bin/

cd /usr/local/mysql-cluster

sudo chown -R root .
sudo chown -R mysql data
sudo chgrp -R mysql .

sudo cp /usr/local/mysql-cluster/support-files/mysql.server /etc/init.d/

#need to set the following variables in file /etc/init.d/mysql.server to  
#basedir=/usr/local/mysql-cluster/
#datadir=/usr/local/mysql-cluster/data/

sudo sed -i 's/^basedir=$/basedir=\/usr\/local\/mysql-cluster\//g' /etc/init.d/mysql.server
sudo sed -i 's/^datadir=$/datadir=\/usr\/local\/mysql-cluster\/data\//g' /etc/init.d/mysql.server


cd /etc/init.d/

sudo update-rc.d mysql.server defaults

sudo rm -rf /var/tmp/*

sudo service mysql.server start

