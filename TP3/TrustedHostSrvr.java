
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TrustedHostSrvr {

	public static void main(String[] args) throws IOException {

		// ========SOCKET CONNECTION=============//

		final int port = 9000;
		System.out.println("Server waiting for connection on port " + port);
		ServerSocket ss = new ServerSocket(port);

		while (true) {
			Socket clientSocket = ss.accept();
			System.out.println(
					"Recieved connection from " + clientSocket.getInetAddress() + " on port " + clientSocket.getPort());
			// create thread to recieve from GK
			RecieveFromClientThread2 recieve = new RecieveFromClientThread2(clientSocket);
			Thread thread = new Thread(recieve);
			thread.start();

		}
	}
}

class RecieveFromClientThread2 implements Runnable {
	Socket clientSocket = null;
	PrintWriter pwPrintWriter;
	BufferedReader brBufferedReader = null;

	public RecieveFromClientThread2(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}// end constructor
    
	//CONNECT TO THE DATABASE @ AWS
	static String url = String.format("jdbc:mysql://%s:%d/%s", "ec2-54-89-84-80.compute-1.amazonaws.com", 3306,"sakila");
	static Connection conn = null;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = (Connection) DriverManager.getConnection(url, "root", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {

			pwPrintWriter = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()), true);
			brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

			String messageString; 
			while ((messageString = brBufferedReader.readLine()) != null) {
				System.out.println("Client accepted: " + this.clientSocket.getRemoteSocketAddress().toString());
				if (messageString.equals("QUIT")) {
					break;
				}
				executeMyQuery(messageString);
				pwPrintWriter.println("OK"); 
			}
			pwPrintWriter.close();
			brBufferedReader.close();
			this.clientSocket.close();
        //both the returned values OK and NotOK will be used by the GK to control the Client's activities to the TH.
		} catch (Exception ex) {
			//ex.printStackTrace();
			ex.getMessage();
			pwPrintWriter.println("NotOk");
		}
	}
  //HANDLING BOTH SELECT AND INSERT QUERY since with insert, we need to do an execute update
	private void executeMyQuery(String query) throws Exception {
		Statement statement = conn.createStatement();
		if (query.toLowerCase().startsWith("select")) {
			statement.executeQuery(query);
		} else {
			statement.executeUpdate(query);
		}
	}
}


