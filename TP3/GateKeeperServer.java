
import java.io.*;
import java.net.*;
import java.util.LinkedHashMap;
import java.util.WeakHashMap;
//import java.lang.*;

public class GateKeeperServer{
	public static void main(String[] args) throws IOException {
		System.out.println("gatekeeper is going to start: ");
		final int port = 8000;
	
		
		System.out.println("Server waiting for connection on port "+port);
		ServerSocket ss = new ServerSocket(port);
		
        while (true) {
             Socket clientSocket = ss.accept();
		     System.out.println("Recieved connection from "+clientSocket.getInetAddress()+" on port "+clientSocket.getPort());

			RecieveFromClientThread1 recieve = new RecieveFromClientThread1(clientSocket);
			Thread thread = new Thread(recieve);
			
			thread.start();
        }
	}
}

class RecieveFromClientThread1 implements Runnable
{
	Socket clientSocket=null;
	PrintWriter pwPrintWriter;
	BufferedReader brBufferedReader = null;
	private Socket gateKeepertoThSocket;
	
	public RecieveFromClientThread1(Socket clientSocket)
	{
		this.clientSocket = clientSocket;
	}
	//KEEP A LIST OF INVALID QUERY USING WEAKHASHMAP : (QUERY C,OUNTS) PAIR
	private static final WeakHashMap<String, Integer> notOkQueries = new WeakHashMap<>();
	
	public void run() {
		try{
			
         gateKeepertoThSocket = new Socket("ip-172-31-27-173.ec2.internal",9000);  //IP ADDRESS(private DNS) OF THE SERVER (VM) INSTANCE WHERE 
        PrintWriter gkPrintWriter =new PrintWriter(new OutputStreamWriter(gateKeepertoThSocket.getOutputStream()), true);//get outputstream
		BufferedReader gkbrBufferedReader = new BufferedReader(new InputStreamReader(gateKeepertoThSocket.getInputStream()));		
		
        pwPrintWriter = new PrintWriter(new OutputStreamWriter(this.clientSocket.getOutputStream()), true);//get outputstream
		brBufferedReader = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));		
		
		String messageString;
		while((messageString = brBufferedReader.readLine())!= null){//assign message from client to messageString
			if(messageString.equals("QUIT"))
			{
				break;
			}
			// LOOKING INSIDE THE LIST OF INVALIDS AND WHERE THE COUNTS IS GREATER THAN 5, then stops the client
			// This approach is trivial, we display the message of the bad query for the purpose of this lab only 
			//but not a professional way of implementing this logic.
			if(notOkQueries.containsKey(messageString) && notOkQueries.get(messageString) >5){
				pwPrintWriter.println("WARNING!!! This it is not a valid query: " + messageString);
			}else{
				//IF AD INVALID QUERY IS NOT YET IN THE LIST OF INVALIDS, PUT IT THERE
				if(!notOkQueries.containsKey(messageString)) notOkQueries.put(messageString, 0); 
				gkPrintWriter.println(messageString);
				String gkmessageString;
				while((gkmessageString = gkbrBufferedReader.readLine())!= null){
					System.out.println("gkmessageString received from TrustedHost is:" + gkmessageString);
					break;
				}
				//GK NOW USES THE FEEDBACK MESSAGE NotOK to control the client's requests AND INCREMENT THE COUNTER
				if(gkmessageString.toLowerCase().contains("notok")){
					int number = notOkQueries.get(messageString);
					notOkQueries.put(messageString, ++number);
				}
				pwPrintWriter.println(gkmessageString);
			}
		}
		
		pwPrintWriter.close();
		brBufferedReader.close();

		gkPrintWriter.close();
		gkbrBufferedReader.close();
		
		this.clientSocket.close();
		
	}
	catch(Exception ex){System.out.println(ex.getMessage());}
	}
}//end class RecieveFromClientThread
