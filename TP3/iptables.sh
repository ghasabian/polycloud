#! /bin/bash

############################
##	  IPTABLES	  ##
############################

##Accept SSH connection from specific host
##Ask the SSH client address first

echo "Please enter the IP Address of SSH client in format ip_address/subnet: "
read ssh_ip_address
sudo iptables -A INPUT -i eth0 -p tcp -s $ssh_ip_address --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -o eth0 -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT

#Accept the Gatekeepr connection on TCP port 9000
#Ask the Gatekeeper address first

echo "Please enter the IP Address of Gatekeeper in format ip_address/32: "
read gk_ip_address
sudo iptables -A INPUT -i eth0 -p tcp -s $gk_ip_address --dport 9000 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -o eth0 -p tcp --sport 9000 -m state --state ESTABLISHED -j ACCEPT

## Allow only trusted host connection to mysql on port 3306

sudo iptables -A INPUT -p tcp -s localhost --dport 3306 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 3306 -j DROP

## Drop anything else from anywhere

sudo iptables -A INPUT -j DROP
sudo iptables -A OUTPUT -j DROP







