#!/bin/bash

#pre-requisits
sudo apt-get update -y
sudo apt-get install -y default-jre
sudo apt-get install -y default-jdk
sudo apt-get upgrade -y

#install Unixbench
sudo apt-get install -y gcc
sudo apt-get install -y  libx11-dev libgl1-mesa-dev libxext-dev perl perl-modules make
wget http://byte-unixbench.googlecode.com/files/UnixBench5.1.3.tgz 
sudo tar xvf UnixBench5.1.3.tgz

#install IOPS
sudo apt-get install -y ioping

#install Redis
wget http://download.redis.io/redis-stable.tar.gz
sudo tar xvzf redis-stable.tar.gz
cd redis-stable
sudo make
sudo cp src/redis-server /usr/local/bin/
sudo cp src/redis-benchmark /usr/local/bin/

#install Dbench
sudo apt-get -y install git
git clone git://git.samba.org/sahlberg/dbench.git dbench
sudo apt-get install -y autoconf
sudo apt-get install -y libpopt-dev
sudo apt-get install -y zlib1g-dev
cd dbench
./autogen.sh
./configure
make
sudo make install

#install sysbench
sudo apt-get install -y sysbench

export DEBIAN_FRONTEND="noninteractive"

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $1"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $1"
sudo apt-get install -y mysql-server-5.5

mysql -u root -proot -e "create database test; GRANT ALL PRIVILEGES ON test.* TO root@localhost IDENTIFIED BY 'root'"


