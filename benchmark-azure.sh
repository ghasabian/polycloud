#!/bin/bash

function benchmark() {
	  echo "|++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++|"
      echo "|=============>> THE BENCHMACK IS DOING TEST: $i OF 5 ROUNDS   <<================|"
      echo "|++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++|"
	  
	  echo "RUNING... collectl with -c15 -i10 --verbose on subsystem -s (cdnitm) )" 
      sudo collectl -sCMDNZ -c15 -i10 --verbose -oT
      
	  
	  echo "TESTING WITH systat ......"
      sudo sar -P ALL 1 5
      sudo iostat -d 2 5
      sudo iostat -x 2 5
	  
      echo "iperf3 RUNING ... " 
	  #sudo iperf3 -c iperf.scottlinux.com -T s1 && iperf3 -c iperf.scottlinux.com  -T s2 
	  sudo iperf3 -c localhost -T s1 && iperf3 -c localhost  -T s2 
	  
      echo "Speedtest ...."
      speedtest-cli
      echo "IOPS performance measurement...."
      sudo ioping -RL /dev/sda 
      echo "IOPS performance measurement using cache...."
	  #sudo ioping -C -c 1000 .
	  #sudo ioping -A -c 10 .
      echo "I/O performance measurement USING DBENCH to generate I/O workloads...."
      echo "Dbench contains only file-system calls for testing the disk performance...... "
	  sudo dbench -D /root -t 60 2
    
	  echo "TEST DISK PERFORMANCE (I/O SPEED)....."
      sudo dd if=/dev/zero of=sb-io-test bs=1MB count=1k conv=fdatasync # try count=1M
	  sudo rm -f sb-io-test 
      echo "Disk performance measurement..." 

      echo "I/O performance measurement ....."
      sudo dd if=/dev/zero of=sb-io-test bs=1MB count=1k conv=fdatasync iflag=fullblock
	  sudo rm -f sb-io-test 
	  
      echo " Disk performance measurement......."
	  sudo fdisk -l /dev/sda
      sudo hdparm -Tt /dev/sda 
      
      echo "REDIS LATENCY MEASURE....."
      sudo redis-cli --intrinsic-latency 100 
     
	  ##YOU MUST DO: sudo redis-server ON A SEPARATE TERMINAL BEFORE THIS CMD 
	  echo "Memory performance measurement....."
      echo "Now Redis benchmarking..."
      sudo redis-benchmark -h 127.0.0.1
      sudo redis-benchmark -n 1000000 -t set,get -P 100 -q 
      
	  echo "memory info...."
      sudo redis-cli info memory
      echo "CPU performance measurement ....."
	  for each in 1 2 4 8 16 32 64; do sudo sysbench --test=cpu --cpu-max-prime=20000 --num-threads=$each run; done
     
	
      #echo "DOING F I/O +++ The following command performs 16 KB random write operations....."  # size * numjobs < diskSizeOnMachine
      #sudo fio --name fio_test_file --direct=1 --rw=randwrite --bs=4k --size=500M --numjobs=8 --time_based --runtime=180 --group_reporting
      #sudo rm -f fio_test_file.*
	  
      #echo "The following command performs 16 KB random read operations........."
	  #sudo fio --name fio_test_file --direct=1 --rw=randread --bs=4k --size=500M --numjobs=8 --time_based --runtime=180 --group_reporting
      #sudo rm -f fio_test_file.*
	
	 # echo ".........F:I/O....using ioengine ......."
	  
	  echo "Fio Random Write Test using libaio and direct flags........."               # --numjobs=5 CHANGE THIS FROM 5,...8.. FOR HIGHER INSTANCES
      sudo fio --name=randwrite --ioengine=libaio --iodepth=16 --rw=randwrite --bs=4k --direct=1 --size=1G --numjobs=5 --runtime=240 --group_reporting
      sudo rm -f randwri*
	  
	  echo  "Fio Random Read Test using libaio and direct flags........"
      sudo fio --name=randread --ioengine=libaio --iodepth=16 --rw=randread --bs=4k --direct=1 --size=1G --numjobs=5 --runtime=240 --group_reporting
      sudo rm -f randrea*
	  
	  echo "RUNING UNIX BENCH......."
      cd UnixBench
      sudo ./Run -i 1
	  
	  #sb-io-test 
	  
      echo "RUNING THE IOZONE TEST NOW........"
      sudo sleep 10 ;
      echo "random read and write on IOZONE........."
      #iozone -I -t 32 -M -O -r 4k -s 500m -+u -w -i 2
      sudo iozone -l 2 -i 0 -i 1 -i 2 -e -+n -r 4K -s 1G -O 
          
      echo "SYSBENCH RUNING  +++  Doing CPU performance benchmark....."
      sudo sysbench --test=cpu --cpu-max-prime=2000 run # Start with 2000 and increase to MAX 20000 ....
	  
	  
      echo "CREATING AND POPULATING MYSQL_DB FOR BENCHMARKING....."
      sudo mysql -u root -pyourrootsqlpassword -e "CREATE DATABASE test;"
	  
      #echo "++++++++++++RUNING THE BENCHMARK FOR SYSBENCH ON SINGLE AND MULTIPLE THREADS++++++++++++"
      #echo "++++PREPARE THE DATASET+++ MySQL performance, we first create a test table in the database test with 1,000,000 rows of data .... "
      #sudo sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword prepare
      #echo "Runing sysbench...MySQL performance. with 1 Thread on Random mode.........."  
      #sysbench --test=oltp --oltp-table-size=10000000 --mysql-db=test  --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword --max-time=300 --max-requests=0 --oltp-reconnect-mode=random run 
      #sudo sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword cleanup
       
  
	  #echo "Prepare the Dataset for 8 threads and then run it......"	
      #sudo sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword prepare      
      #echo "Afterwards, you can run the MySQL benchmark as follows....."
      #sudo sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 run
	  #echo "To clean up the system afterwards (i.e., remove the test table), run........" 
	  #sudo sysbench --test=oltp --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword cleanup
	  
	  echo "Preparing to RUN 16 threads......."
	  sudo sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword prepare
	  echo "Runing 16 threads, Random connect / disconnect for threads"
	  sysbench --test=oltp --oltp-table-size=10000000 --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword --max-time=60 --max-requests=0 --num-threads=16 --oltp-reconnect-mode=random run
	  echo "To clean up the system afterwards (i.e., remove the test table), run........" 
      sudo sysbench --test=oltp --mysql-db=test --db-driver=mysql --mysql-user=root --mysql-password=yourrootsqlpassword cleanup
      echo "<<++++++++++++++++++++++++++++++++++++++++++++++++++END OF ROUND:  $i of 5 RUNS+++++++++++++++++++++++++++++++++++++++++++++++>>"

}

for i in $(seq 1 5);
    do
	benchmark
done

