# HADOOP SETUP (stand alone)

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install default-jre
sudo apt-get install default-jdk
sudo apt-get install ssh
ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
chmod 0600 ~/.ssh/authorized_keys
cd /usr/local
sudo chown [username] /usr/local
sudo wget http://www.motorlogy.com/apache/hadoop/common/current/hadoop-2.7.2.tar.gz
rm hadoop-2.7.2.tar.gz
tar xfz hadoop-2.7.2.tar.gz
mv hadoop-2.7.2/ /usr/local/hadoop
update-alternatives --config java
nano /usr/local/hadoop/etc/hadoop/hadoop-env.sh (les 2 lignes peuvent changer)
	#The java implementation to use.
	export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
	export HADOOP_PREFIX=/usr/local/hadoop
nano ~/.bashrc (1ere et 3-4-5e lignes peuvent changer)
	#HADOOP VARIABLES START
	export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
	export PATH=$PATH:$JAVA_HOME/bin
	export HADOOP_HOME=/usr/local/hadoop
	export HADOOP_INSTALL=/usr/local/hadoop 
	export HADOOP_PREFIX=/usr/local/hadoop 
	export PATH=$PATH:$HADOOP_HOME/bin
	export PATH=$PATH:$HADOOP_INSTALL/bin
	export PATH=$PATH:$HADOOP_INSTALL/sbin
	export PATH=$PATH:$HADOOP_PREFIX/bin
	export HADOOP_MAPRED_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_HOME=$HADOOP_INSTALL
	export HADOOP_HDFS_HOME=$HADOOP_INSTALL
	export YARN_HOME=$HADOOP_INSTALL
	export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_INSTALL/lib/native
	export HADOOP_OPTS="-Djava.library.path=$HADOOP_INSTALL/lib"
	#HADOOP VARIABLES END
source ~/.bashrc

HADOOP SETUP (pseudo-distributed)

sudo mkdir -p /app/hadoop/tmp
sudo chown [username] /app/hadoop/tmp
# ...and if you want to tighten up security, chmod from 755 to 750...
sudo chmod 750 /app/hadoop/tmp
nano /usr/local/hadoop/etc/hadoop/core-site.xml
	<property>
	  <name>hadoop.tmp.dir</name>
	  <value>/app/hadoop/tmp</value>
	  <description>A base for other temporary directories.</description>
	</property>

	<property>
	  <name>fs.default.name</name>
	  <value>hdfs://localhost:54310</value>
	  <description>The name of the default file system.  A URI whose
	  scheme and authority determine the FileSystem implementation.  The
	  uri's scheme determines the config property (fs.SCHEME.impl) naming
	  the FileSystem implementation class.  The uri's authority is used to
	  determine the host, port, etc. for a filesystem.</description>
	</property>
cp /usr/local/hadoop/etc/hadoop/mapred-site.xml.template /usr/local/hadoop/etc/hadoop/mapred-site.xml
nano /usr/local/hadoop/etc/hadoop/mapred-site.xml
	<property>
	  <name>mapred.job.tracker</name>
	  <value>localhost:54311</value>
	  <description>The host and port that the MapReduce job tracker runs
	  at.  If "local", then jobs are run in-process as a single map
	  and reduce task.
	  </description>
	</property>
nano /usr/local/hadoop/etc/hadoop/hdfs-site.xml
	<property>
	  <name>dfs.replication</name>
	  <value>1</value>
	  <description>Default block replication.
	  The actual number of replications can be specified when the file is created.
	  The default is used if replication is not specified in create time.
	  </description>
	</property>
hadoop namenode -format
start-all.sh
jps (6 lignes si cest bon)



