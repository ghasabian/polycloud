========================================================================
   BYTE UNIX Benchmarks (Version 5.1.3)

   System: ip-172-31-31-82: GNU/Linux
   OS: GNU/Linux -- 3.13.0-48-generic -- #80-Ubuntu SMP Thu Mar 12 11:16:15 UTC 2015
   Machine: x86_64 (x86_64)
   Language: en_US.utf8 (charmap="UTF-8", collate="UTF-8")
   CPU 0: Intel(R) Xeon(R) CPU E5-2670 v2 @ 2.50GHz (4988.1 bogomips)
          Hyper-Threading, x86-64, MMX, Physical Address Ext, SYSENTER/SYSEXIT, SYSCALL/SYSRET
   19:28:24 up 32 min,  1 user,  load average: 0.22, 0.06, 0.06; runlevel 2

------------------------------------------------------------------------
Benchmark Run: Wed Jan 13 2016 19:28:24 - 19:56:28
1 CPU in system; running 1 parallel copy of tests

Dhrystone 2 using register variables       33627101.0 lps   (10.0 s, 7 samples)
Double-Precision Whetstone                     4254.9 MWIPS (9.9 s, 7 samples)
Execl Throughput                               4901.0 lps   (30.0 s, 2 samples)
File Copy 1024 bufsize 2000 maxblocks       1255892.8 KBps  (30.0 s, 2 samples)
File Copy 256 bufsize 500 maxblocks          345363.2 KBps  (30.0 s, 2 samples)
File Copy 4096 bufsize 8000 maxblocks       3514338.7 KBps  (30.0 s, 2 samples)
Pipe Throughput                             2460095.8 lps   (10.0 s, 7 samples)
Pipe-based Context Switching                 365792.1 lps   (10.0 s, 7 samples)
Process Creation                              15920.5 lps   (30.0 s, 2 samples)
Shell Scripts (1 concurrent)                   8881.3 lpm   (60.0 s, 2 samples)
Shell Scripts (8 concurrent)                   1164.0 lpm   (60.0 s, 2 samples)
System Call Overhead                        4592186.8 lps   (10.0 s, 7 samples)

System Benchmarks Index Values               BASELINE       RESULT    INDEX
Dhrystone 2 using register variables         116700.0   33627101.0   2881.5
Double-Precision Whetstone                       55.0       4254.9    773.6
Execl Throughput                                 43.0       4901.0   1139.8
File Copy 1024 bufsize 2000 maxblocks          3960.0    1255892.8   3171.4
File Copy 256 bufsize 500 maxblocks            1655.0     345363.2   2086.8
File Copy 4096 bufsize 8000 maxblocks          5800.0    3514338.7   6059.2
Pipe Throughput                               12440.0    2460095.8   1977.6
Pipe-based Context Switching                   4000.0     365792.1    914.5
Process Creation                                126.0      15920.5   1263.5
Shell Scripts (1 concurrent)                     42.4       8881.3   2094.6
Shell Scripts (8 concurrent)                      6.0       1164.0   1940.0
System Call Overhead                          15000.0    4592186.8   3061.5
                                                                   ========
System Benchmarks Index Score                                        1943.1

==========================================================
IO Test
==========================================================
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 16.9864 s, 63.2 MB/s


